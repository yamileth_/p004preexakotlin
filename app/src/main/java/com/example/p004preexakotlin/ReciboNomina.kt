package com.example.p004preexakotlin

class ReciboNomina(
    var numRecibo: Int,
    var nombre: String,
    var horasTrabNormal: Float,
    var horasTrabExtras: Float,
    var puesto: Int,
    var impuestoPorc: Float
) {
    fun calcularSubtotal(): Float {
        var pagoBase = 200f
        var incrementoPago = 0f
        var pagoPorHora = 0f

        // Calcular el incremento del pago según el puesto
        when (puesto) {
            1 -> incrementoPago = 0.2f
            2 -> incrementoPago = 0.5f
            3 -> incrementoPago = 1f
        }

        // Calcular el incremento del pago base
        pagoBase += pagoBase * incrementoPago

        // Calcular el pago por horas normales
        val pagoHorasNormales = pagoBase * horasTrabNormal

        // Calcular el pago por horas extras
        if (incrementoPago > 0) {
            pagoPorHora = pagoBase
        }
        val pagoHorasExtras = pagoPorHora * horasTrabExtras * 2

        // Calcular el subtotal
        val subtotal = pagoHorasNormales + pagoHorasExtras

        return subtotal
    }

    fun calcularImpuesto(): Float {
        val subtotal = calcularSubtotal()
        impuestoPorc = 0.16f
        val impuesto = subtotal * impuestoPorc
        return impuesto
    }

    fun calcularTotal(): Float {
        val subtotal = calcularSubtotal()
        val impuesto = calcularImpuesto()
        val total = subtotal - impuesto
        return total
    }
}