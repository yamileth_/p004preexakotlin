package com.example.p004preexakotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity


class ActivityRecibo : AppCompatActivity() {

    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var txtHorasTrabajadas: EditText
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtHorasExtras: EditText
    private lateinit var lblSubtotal: TextView
    private lateinit var lblImpuesto: TextView
    private lateinit var lblTotal: TextView
    private lateinit var rdbAuxiliar: RadioButton
    private lateinit var rdbAlbanil: RadioButton
    private lateinit var txtNombre: EditText
    private lateinit var rdbIngObra: RadioButton

    private val recibo = ReciboNomina(0, "", 0f, 0f, 0, 0f)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo)

        iniciarComponentes()
        val bundle = intent.extras
        txtNombre.setText(bundle!!.getString("Nombre"))

        rdbAuxiliar.setOnClickListener { Auxiliar() }
        rdbAlbanil.setOnClickListener { Albanil() }
        rdbIngObra.setOnClickListener { IngObra() }
        btnRegresar.setOnClickListener { regresar() }
        btnLimpiar.setOnClickListener { limpiar() }
        btnCalcular.setOnClickListener { calcular() }
    }

    private fun iniciarComponentes() {
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtHorasExtras = findViewById(R.id.txtHorasExtras)
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas)
        lblSubtotal = findViewById(R.id.lblSubtotal)
        lblImpuesto = findViewById(R.id.lblImpuesto)
        lblTotal = findViewById(R.id.lblTotal)
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar)
        rdbAlbanil = findViewById(R.id.rdbAlbanil)
        rdbIngObra = findViewById(R.id.rdbIngObra)
        txtNombre = findViewById(R.id.txtNombre)
    }

    private fun Auxiliar() {
        recibo.puesto = 1
    }

    private fun Albanil() {
        recibo.puesto = 2
    }

    private fun IngObra() {
        recibo.puesto = 3
    }

    private fun limpiar() {
        // Limpia los campos de entrada y los valores de lblSubtotal, lblImpuesto y lblTotal
        txtNumRecibo.setText("")
        txtHorasTrabajadas.setText("")
        txtHorasExtras.setText("")
        lblSubtotal.text = ""
        lblImpuesto.text = ""
        lblTotal.text = ""
        rdbAuxiliar.isChecked = false
        rdbAlbanil.isChecked = false
        rdbIngObra.isChecked = false
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Cálculo de nómina")
        confirmar.setMessage("¿Desea regresar?")
        confirmar.setPositiveButton("Confirmar") { _, _ -> finish() }
        confirmar.setNegativeButton("Cancelar", null)
        confirmar.show()
    }

    private fun calcular() {
        val numReciboStr = txtNumRecibo.text.toString().trim()
        val horasTrabajadasStr = txtHorasTrabajadas.text.toString().trim()
        val horasExtrasStr = txtHorasExtras.text.toString().trim()

        if (numReciboStr.isNotEmpty() && horasTrabajadasStr.isNotEmpty() && horasExtrasStr.isNotEmpty() && (rdbAuxiliar.isChecked || rdbAlbanil.isChecked || rdbIngObra.isChecked)) {
            val numRecibo = numReciboStr.toInt()
            val horasTrabajadas = horasTrabajadasStr.toFloat()
            val horasExtras = horasExtrasStr.toFloat()

            recibo.numRecibo = numRecibo
            recibo.horasTrabNormal = horasTrabajadas
            recibo.horasTrabExtras = horasExtras

            val subtotal = recibo.calcularSubtotal()
            val impuesto = recibo.calcularImpuesto()
            val total = recibo.calcularTotal()

            lblSubtotal.text = subtotal.toString()
            lblImpuesto.text = impuesto.toString()
            lblTotal.text = total.toString()
        } else {
            Toast.makeText(this, "Por favor, asegurese de llenar todos los campos", Toast.LENGTH_SHORT).show()
        }
    }

}

//terminacion del proyecto